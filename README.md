# TOOLROOT_20

This is the C++20 TOOLROOT. It is big and filled with binaries. Generally you do not want a full clone of the repository but just want to pull a branch specific to your system.

## Branches

The **main** branch is useless except to read this file and figure out where you really want to go.

The **invariant** branch is the place to put everything that will be shared between all other platform branches. Do not put anything in the **invariant** that can not be shared by all platforms! This branch can be merged into platform branches at anytime.

Next are the platforms, **ubuntu20**, **centos7** and **amazonlinux2** for example. These are the ones to put on your machine for use.

### Cloning a Branch

Go to the directory where you want to place the TOOLROOT and just clone the branch:

```sh
cd /opt/hydra
git clone -b ubuntu20 git@frqstic:peercache/toolroot/platform/toolroot_20.git TOOLROOT_20
```

This gets the branch from the private repository. Similarly, it can be pulled from the public repository:

```sh
cd /opt/cadroot
git clone -b ubuntu20 https://gitlab.com/rogmon/toolroot_20.git TOOLROOT_20
```

This will be a read-only version.
